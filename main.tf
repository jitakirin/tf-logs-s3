resource "aws_s3_bucket" "logs_bucket" {
  bucket = "${var.bucket_name}"
  acl = "log-delivery-write"

  tags {
    Name = "${var.bucket_name}"
    description = "Logs storage for S3"
    project = "global"
    env = "dev"
  }

  lifecycle_rule {
    enabled = true

    tags {
      "rule" = "log"
      "autoclean" = "true"
    }

    transition {
      days = "${var.transition_to_std_ia_after}"
      storage_class = "STANDARD_IA"
    }

    transition {
      days = "${var.transition_to_glacier_after}"
      storage_class = "GLACIER"
    }

    expiration {
      days = "${var.expire_after}"
    }
  }
}
