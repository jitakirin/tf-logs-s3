# tf-logs-s3

Terraform config to setup a log storage S3 bucket.

Based on Terraform docs
[for S3 bucket](https://www.terraform.io/docs/providers/aws/r/s3_bucket.html).
Sets up a bucket with `log-delivery-write` canned ACL and with
lifecycle rules to transition logs to slower storage classes and
eventually expire them.

# Variables

The following variables are available:

<dl>
  <dt>bucket_name (required)</dt>
  <dd>Name of the bucket to create</dd>
  <dt>transition_to_std_ia_after (optional, def: 30)</dt>
  <dd>Transition to Standard IA storage class after specified number of days</dd>
  <dt>transition_to_glacier_after (optional, def: 60)</dt>
  <dd>Transition to Glacier storage class after specified number of days</dd>
  <dt>expire_after (optional, def: 90)</dt>
  <dd>Expire objects after specified number of days</dd>
</dl>

# Usage

```sh
terraform apply --var bucket_name=my-s3-logs
```
