variable "bucket_name" {
  description = "Name of the bucket to create"
}

variable "transition_to_std_ia_after" {
  description = "Transition to Standard IA storage class after number of days (default: 30)"
  default = 30
}

variable "transition_to_glacier_after" {
  description = "Transition to Glacier storage class after number of days (default: 60)"
  default = 60
}

variable "expire_after" {
  description = "Expire objects after number of days (defaul: 90)"
  default = 90
}
