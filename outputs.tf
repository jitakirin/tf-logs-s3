output "bucket_id" {
  description = "Name of the bucket created for logs"
  value = "${aws_s3_bucket.logs_bucket.id}"
}

output "bucket_domain_name" {
  description = "Domain name of the created bucket"
  value = "${aws_s3_bucket.logs_bucket.bucket_domain_name}"
}
